# Gitlab Pipline templates

This project contains a file with all templates and needed variables.

## Usage
Choose a job to include in build stage in order to successfully reach the desired result.

```yaml
stages:
- build
```
Add needed variables and files inside the Repo and inside the CICD variables in gitlab.
Create .gitlab-ci.yml file and add to the project with the desired stages included.
## Examples:
```yaml
## Build Stage

### Build React To static files
#### Variables None
#### Artifacts build/ & build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/nodejs.yml'
### Build Golang to binary
#### Variables None
#### Artifacts output/ & build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/golang.yml'
### Build dotnet 6.0 Binaries
#### Variables:
    # INTERNAL_ARTIFACTORY_USERNAME username for nuget server
    # INTERNAL_ARTIFACTORY_PASSWORD   password for nuget server
    # PROJECT   Project folder
    # INTERNAL_ARTIFACTORY_REPO repository name for nuget server
#### Artifacts publish/ & build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/dotnet_6.0.yml'
### Generate dotnet version (no build)
#### Variables None
#### Artifacts build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/dotnet-versioning.yml'
### Build java 8 maven
#### Variables:
    #   SETTINGS_XML: # file variable containing settings.xml with creds
    #   MAVEN_CLI_OPTS: "-B -s .m2/settings.xml"
    #   MAVEN_TESTS: -DskipTests=true
#### Artifacts target/*.jar & build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/java-maven-openjdk-8-builder.yml'
### Build java 11 maven
#### Variables:
    #   SETTINGS_XML: # file variable containing settings.xml with creds
    #   MAVEN_CLI_OPTS: "-B -s .m2/settings.xml"
    #   MAVEN_TESTS: -DskipTests=true
#### Artifacts target/*.jar & build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/java-maven-openjdk-11-builder.yml'
### Generate Java MAVEN Version (no build) 
#### Variables :
    #   SETTINGS_XML: # file variable containing settings.xml with creds
#### Artifacts build.env
  - project: 'https://gitlab.com/teeco-DevOps/cicd/gitlab-templates/builders'
    ref: master
    file: '/java-versioning.yml'
```